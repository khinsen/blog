# Konrad Hinsen's blog

This repository contains the source code for [my blog](https://blog.khinsen.net/).

## Licenses

The content of the blog (directories `pages`, `posts`, `static`, and `themes`) is covered by the [Creative Commons CC-BY license](https://creativecommons.org/licenses/by/4.0/). See [LICENSE-posts-and-pages.txt](./LICENSE-posts-and-pages.txt).

The code that creates the publishd HTML version is covered by the [GNU Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) (or later). See [LICENSE-code.txt](./LICENSE-code.txt). It integrates code from [coleslaw](https://github.com/coleslaw-org/coleslaw), which is covered by a BSD-type license. See file [LICENSE-coleslaw.txt](./LICENSE-coleslaw.txt).
