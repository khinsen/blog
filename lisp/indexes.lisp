;;;; Tags and month labels
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

;;
;; Index class
;;

(defclass index ()
  ((url     :initarg :url     :accessor page-url)
   (name    :initarg :name    :reader index-name)
   (title   :initarg :title   :reader title-of)
   (content :initarg :content :reader index-content)))

(defmethod content-date ((content index))
  nil)

(defmethod hv:text-representation ((index index))
  (index-name index))

(hv:defview 👀items (index index)
  (-> index index-content hv:👀items))

;;; Index by Tag

(defclass tag-index (index) ())

(defun make-tag-index (blog)
  (let ((content (posts-sorted-by-date blog)))
    (dolist (tag (blog-tags blog))
      (add-document blog (index-by-tag blog tag content)))))

(defun index-by-tag (blog tag content)
  "Return an index of all CONTENT matching the given TAG."
  (let* ((index
           (make-instance 'tag-index
                          :name (tag-name tag)
                          :content (remove-if-not (lambda (x) (tag-p tag x)) content)
                          :title (format nil "Posts tagged ~a" (tag-name tag))))
         (url (compute-url blog index (tag-slug tag))))
    (setf (page-url index) url)
    (setf (tag-url tag) url)
    index))

(defun tag-p (tag obj)
  "Test if OBJ is tagged with TAG."
  (let ((tag (if (typep tag 'tag) tag (make-tag tag))))
    (member tag (content-tags obj) :test #'tag-slug=)))

;;; Index by Month

(defclass month-index (index) ())

(defun make-month-index (blog)
  (let ((content (posts-sorted-by-date blog)))
    (dolist (month (blog-months blog))
      (add-document blog (index-by-month blog month content)))))

(defun index-by-month (blog month content)
  "Return an index of all CONTENT matching the given MONTH."
  (let ((index
          (make-instance 'month-index
                         :name month
                         :content (remove-if-not (lambda (x) (month-p month x)) content)
                         :title (format nil "Posts from ~a" month))))
    (with-slots (url) index
      (setf url (compute-url blog index month)))
    index))

(defun month-p (month obj)
  "Test if OBJ was written in MONTH."
  (search month (content-date obj)))

;;; Reverse Chronological Index

(defclass numeric-index (index) ())

(defun make-reverse-chronological-index (blog)
  (let ((content (posts-sorted-by-date blog)))
    (dotimes (i (ceiling (length content) 10))
      (add-document blog (index-by-n blog i content)))))

(defun index-by-n (blog i content)
  "Return the index for the Ith page of CONTENT in reverse chronological order."
  (let* ((content (subseq content (* 10 i)))
         (index
           (make-instance 'numeric-index
                          :name (format nil "~a" (1+ i))
                          :content (take-up-to 10 content)
                          :title "Recent posts")))
    (with-slots (url) index
      (setf url (compute-url blog index (1+ i))))
    index))

(defun take-up-to (n seq)
  "Take elements from SEQ until all elements or N have been taken."
  (subseq seq 0 (min (length seq) n)))
