;;;; Feeds
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

;;
;; Feed class
;;

(defclass feed (index)
  ((format :initarg :format :reader feed-format)))

(defmethod hv:text-representation ((feed feed))
  (symbol-name (feed-format feed)))

(defun make-feeds (blog)
  (let ((content (posts-sorted-by-date blog)))
    (dolist (format '(rss atom))
      (let ((feed (make-instance 'feed
                                 :format format
                                 :content (take-up-to 100 content))))
        (with-slots (url) feed
          (setf url (compute-url blog feed (format nil "~(~a~)" format))))
        (add-document blog feed)))))
