;;;; Load pages and posts
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

(defun add-content (blog subdir class)
  (dolist (file (source-files blog subdir))
    (->> file
         (read-content blog)
         (apply #'make-instance class)
         (compute-metadata blog)
         (add-document blog))))

(defun source-files (blog subdir)
  (->> blog
       blog-dir
       (merge-pathnames subdir)
       uiop:directory-files))

;;
;; Coleslaw code with minor modifictions
;;

(defun read-content (blog file)
  "Returns a plist of metadata from FILE with :text holding the content."
  (flet ((slurp-remainder (stream)
           (let ((seq (make-string (- (file-length stream)
                                      (file-position stream)))))
             (read-sequence seq stream)
             (remove #\Nul seq))))
    (with-open-file (in file :external-format :utf-8)
      (let ((metadata (parse-metadata (blog-metadata-sep blog) in))
            (content (slurp-remainder in))
            (filepath (enough-namestring file (blog-dir blog))))
        ;; discard the time part of the date
        (setf (getf metadata :date)
              (first (uiop:split-string (getf metadata :date))))
        (append metadata
                (list :text content
                      :file filepath
                      :format (-> file
                                  pathname-type
                                  str:upcase
                                  alexandria:make-keyword)))))))

(defun parse-metadata (separator stream)
  "Given a STREAM, parse metadata from it or signal an appropriate condition."
  (flet ((get-next-line (input)
           (string-trim '(#\Space #\Return #\Newline #\Tab) (read-line input nil))))
    (unless (string= (get-next-line stream) separator)
      (error "The file, ~a, lacks the expected header: ~a"
             (file-namestring stream) separator))
    (loop for line = (get-next-line stream)
          until (string= line separator)
          appending (parse-initarg line))))

(defun parse-initarg (line)
  "Given a metadata header, LINE, parse an initarg name/value pair from it."
  (let ((name (string-upcase (subseq line 0 (position #\: line))))
        (match (nth-value 1 (ppcre:scan-to-strings "[a-zA-Z]+:\\s+(.*)" line))))
    (when match
      (list (alexandria:make-keyword name) (aref match 0)))))

(defun add-document (blog document)
  "Add DOCUMENT to the in-memory database. Error if a matching entry is present."
  (let ((content (blog-content blog))
        (url (page-url document)))
    (if (gethash url content)
        (error "There is already an existing document with the url ~a" url)
        (setf (gethash url content) document))))

(defgeneric compute-metadata (blog content))

(defmethod compute-metadata (blog (content content))
  (with-slots (tags) content
    (when (stringp tags)
      (setf tags (mapcar #'make-tag (ppcre:split "," tags)))))
  content)

(defmethod compute-metadata (blog (post post))
  (with-slots (url title author excerpt format text) post
    (let (post-content)
      (setf url (compute-url blog post (basename post))
            format (alexandria:make-keyword (string-upcase format))
            post-content (render-text text format)
            excerpt (or excerpt
                        (first (ppcre:split (blog-excerpt-sep blog)
                                            post-content
                                            :limit 2)))
            text post-content
            author (or author (blog-author blog)))))
  (call-next-method))

(defmethod compute-metadata (blog (page page))
  (with-slots (url text format title) page
    (setf url (compute-url blog page (pathname-name (content-file page)))
          format (alexandria:make-keyword (string-upcase format))
          text (render-text text format)))
  (call-next-method))

(defmethod compute-metadata (blog (draft draft))
  (with-slots (url text format title) draft
    (setf url (compute-url blog draft (pathname-name (content-file draft)))
          format (alexandria:make-keyword (string-upcase format))
          text (render-text text format)))
  (call-next-method))

(defun compute-url (blog document unique-id &optional class)
  "Compute the relative URL for a DOCUMENT based on its UNIQUE-ID. If CLASS
is provided, it overrides the route used."
  (let* ((class-name (or class (class-name (class-of document))))
         (route (get-route blog class-name)))
    (unless route
      (error "No routing method found for: ~A" class-name))
    (let* ((result (format nil route unique-id))
           (type (or (pathname-type result) "html")))
      (make-pathname :name (pathname-name result)
                     :type type
                     :defaults result))))

(defun get-route (blog doc-type)
  "Return the route format string for DOC-TYPE."
  (second (assoc (alexandria:make-keyword doc-type) (blog-routes blog))))

;; This function assumes that the input files follow the naming
;; scheme of Frog, i.e. yyyy-mm-dd-slug-derived-from-title.post.

(defun basename (post)
  (let* ((input-basename (pathname-name (content-file post)))
         (date-part (subseq input-basename 0 10))
         (title-part (subseq input-basename 11)))
    (format nil "~a/~a"
            (substitute #\/ #\- date-part)
            title-part)))

(defgeneric render-text (text format)
  (:documentation "Render TEXT of the given FORMAT to HTML for display.")
  (:method (text (format (eql :html)))
    text)
  (:method (text (format (eql :md)))
    (let ((3bmd-code-blocks:*code-blocks* t))
      (with-output-to-string (str)
        (3bmd:parse-string-and-print-to-stream text str)))))
