;;;; Blog content: pages, posts
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

;;
;; Content class and subclasses for pages and posts
;;

(defclass content ()
  ((url  :initarg :url  :reader page-url)
   (date :initarg :date :reader content-date)
   (file :initarg :file :reader content-file)
   (tags :initarg :tags :reader content-tags)
   (text :initarg :text :reader content-text))
  (:default-initargs :tags nil :date nil))

(defclass post (content)
  ((title  :initarg :title  :reader title-of)
   (author :initarg :author :reader author-of)
   (excerpt :initarg :excerpt :reader excerpt-of)
   (format :initarg :format :reader page-format)
   (toot :initarg :toot :reader toot)
   (doi :initarg :doi :reader doi))
  (:default-initargs :author nil :excerpt nil :toot nil))

(defclass page (content)
  ((title :initarg :title :reader title-of)
   (format :initarg :format :reader page-format))
  (:default-initargs :format :md))

(defclass draft (content)
  ((title :initarg :title :reader title-of)
   (format :initarg :format :reader page-format))
  (:default-initargs :format :md))

;;
;; Views
;;

(defmethod hv:text-representation ((post post))
  (title-of post))

(defmethod hv:text-representation ((page page))
  (title-of page))

(defmethod hv:text-representation ((draft draft))
  (title-of draft))

(defmethod hv:title-bar-action-buttons ((post post))
  (hv:eval-button "Check DOI"
                  (hv:thunk (check-doi post (blog-domain *blog*)))))

(hv:defview 👀source (content content)
  (-> content
      content-file
      (merge-pathnames (blog-dir *blog*))
      hv:👀content
      (hv:rename :title "Source" :priority 1)))

(hv:defview 👀preview (content content)
  (hv:html-view :title "Preview" :priority 2
    (hv:html (hv:str (content-text content)))))

(hv:defview 👀html (content content)
  (hv:html-code-view (hv:thunk (content-text content))
                     :title "HTML" :priority 3))

(hv:defview 👀tags (content content)
  (when-let (tags (content-tags content))
    (hv:list-view tags :title "Tags" :priority 3)))
