;;;; Tags and month labels
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

;;
;; Tag class
;;

(defclass tag ()
  ((name :initarg :name :reader tag-name)
   (slug :initarg :slug :reader tag-slug)
   (url  :initarg :url :accessor tag-url)))

(defmethod hv:text-representation ((tag tag))
  (tag-name tag))

;;
;; Collect tags and months
;;
(defun collect-tags (blog)
  (setf (blog-tags blog) (all-tags blog))
  (setf (blog-months blog) (all-months blog)))

;;
;; Coleslaw code with minor modifications
;;

(defun make-tag (str)
  "Takes a string and returns a TAG instance with a name and slug."
  (let ((trimmed (string-trim " " str)))
    (make-instance 'tag :name trimmed :slug (slugify trimmed))))

(defun slugify (string)
  "Return a version of STRING suitable for use as a URL."
  (let ((slugified (remove-if-not #'slug-char-p
                                  (substitute-if #\- #'unicode-space-p string))))
	  (if (zerop (length slugified))
        (error "Post title '~a' does not contain characters suitable for a slug!" string )
        slugified)))

(defun slug-char-p (char &key (allowed-chars (list #\- #\~)))
  "Determine if CHAR is a valid slug (i.e. URL) character."
  ;; use the first char of the general unicode category as kind of
  ;; hyper general category
  (let ((cat (char (cl-unicode:general-category char) 0))
        (allowed-cats (list #\L #\N))) ; allowed Unicode categories in URLs
        (cond
          ((member cat allowed-cats)   t)
          ((member char allowed-chars) t)
          (t nil))))

(defun unicode-space-p (char)
  "Determine if CHAR is a kind of whitespace by unicode category means."
  (char= (char (cl-unicode:general-category char) 0) #\Z))

(defun all-months (blog)
  "Retrieve a list of all months with published content."
  (let ((months (loop for post in (find-content-of-type blog 'post)
                      for content-date := (content-date post)
                      when content-date
                      collect (subseq content-date 0
                                      (min 7 (length content-date))))))
    (sort (remove-duplicates months :test #'string=) #'string>)))

(defun all-tags (blog)
  "Retrieve a list of all tags used in content."
  (let* ((dupes (alexandria:mappend #'content-tags (find-content-of-type blog 'post)))
         (tags (remove-duplicates dupes :test #'tag-slug=)))
    (sort tags #'string< :key #'tag-name)))

(defun tag-slug= (a b)
  "Test if the slugs for tag A and B are equal."
  (string= (tag-slug a) (tag-slug b)))
