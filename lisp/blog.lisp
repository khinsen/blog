;;;; Blog class, singleton instance, and configuration
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

;;
;; Blog class
;;

(defclass blog ()
  ;; Configuration
  ((title :accessor blog-title :initarg :title)
   (author :accessor blog-author :initarg :author)
   (author_uri :accessor blog-author_uri :initarg :author_uri)
   (author_fedi_handle :accessor blog-author_fedi_handle :initarg :author_fedi_handle)
   (domain :accessor blog-domain :initarg :domain)
   (blog-dir :accessor blog-dir :initarg :blog-dir)
   (metadata-sep :accessor blog-metadata-sep :initarg :metadata-sep)
   (excerpt-sep :accessor blog-excerpt-sep :initarg :excerpt-sep)
   (site-dir :accessor blog-site-dir :initarg :site-dir)
   (routes :accessor blog-routes :initarg :routes)
   ;; State accumulated during processing
   (content :accessor blog-content :initform (make-hash-table :test #'equal))
   (tags :accessor blog-tags :initform nil)
   (months :accessor blog-months :initform nil)))

;;
;; Singleton instance
;;

(defvar *blog*
  (make-instance 'blog
                 :title "Konrad Hinsen's blog"
                 :author "Konrad Hinsen"
                 :author_uri "https://orcid.org/0000-0003-0330-9428"
                 :author_fedi_handle "@khinsen@scholar.social"
                 :domain "https://blog.khinsen.net/"
                 :blog-dir (asdf:system-source-directory :kh-blog)
                 :metadata-sep ";;;;;"
                 :excerpt-sep "<!-- more -->"
                 :site-dir (asdf:system-relative-pathname :kh-blog "site/")
                 :routes '((:post           "posts/~a")
                           (:page           "pages/~a")
                           (:draft           "drafts/~a")
                           (:tag-index      "tag/~a")
                           (:month-index    "date/~a")
                           (:numeric-index  "~d")
                           (:feed           "feeds/all.~a.xml")
                           (:tag-feed       "tag/~a.xml")
                           (:sitemap        "~a.xml"))))

;;
;; High-level operations
;;

(defun reset-blog ()
  (setf (blog-content *blog*) (make-hash-table :test #'equal))
  (setf (blog-tags *blog*) nil)
  (setf (blog-months *blog*) nil)
  *blog*)

(defun load-blog ()
  (reset-blog)
  (compile-templates)
  (add-content *blog* "pages/" 'page)
  (add-content *blog* "posts/" 'post)
  (add-content *blog* "drafts/" 'draft)
  (collect-tags *blog*)
  (make-tag-index *blog*)
  (make-month-index *blog*)
  (make-reverse-chronological-index *blog*)
  (make-feeds *blog*)
  *blog*)

(defun build-blog ()
  (publish-blog *blog*)
  (ensure-preview-server *blog* 4242)
  *blog*)

;;
;; Find content
;;

(defun find-content-of-type (blog content-type)
  (loop for doc being the hash-values in (blog-content blog)
        when (typep doc content-type) collect doc))

(defun posts-sorted-by-date (blog)
  (sort (find-content-of-type blog 'post)
        #'string> :key #'content-date))

;;
;; Views
;;

(defmethod hv:text-representation ((blog blog))
  (blog-title blog))

(defmethod hv:title-bar-action-buttons ((blog blog))
  (hv:action-button "Reload"
                    (hv:thunk (load-blog)
                              t))
  (hv:action-button "Build"
                    (hv:thunk (build-blog)
                              t))
  (hv:action-button "Preview"
                    (hv:thunk (clog:open-browser :url "http://127.0.0.1:4242/")
                              nil))
  (hv:action-button "Publish"
                    (hv:thunk (copy-to-server *blog*)
                              nil)))

(hv:defview 👀posts (blog blog)
  (when-let (posts (posts-sorted-by-date blog))
    (-> posts
        (hv:multi-column-list-view
         :title "Posts" :priority 1
         :columns '("Date" "Title")
         :display (list #'content-date #'title-of)))))

(hv:defview 👀pages (blog blog)
  (when-let (pages (find-content-of-type blog 'page))
    (-> pages
        (hv:list-view :title "Pages" :priority 2
                      :display #'title-of))))

(hv:defview 👀drafts (blog blog)
  (when-let (pages (find-content-of-type blog 'draft))
    (-> pages
        (hv:list-view :title "Drafts" :priority 3
                      :display #'title-of))))

(hv:defview 👀tags (blog blog)
  (when-let (tags (find-content-of-type blog 'tag-index))
    (-> tags
        (hv:list-view :title "Tags" :priority 4
                      :display #'index-name))))

(hv:defview 👀month (blog blog)
  (when-let (tags (find-content-of-type blog 'month-index))
    (-> tags
        (hv:list-view :title "Months" :priority 5
                      :display #'index-name))))

(hv:defview 👀recent (blog blog)
  (when-let (tags (find-content-of-type blog 'numeric-index))
    (-> tags
        (hv:list-view :title "Recent" :priority 6
                      :display #'index-name))))

(hv:defview 👀source-files (blog blog)
  (-<> blog
       blog-dir
       uiop:subdirectories
       (remove-if-not
        #'(lambda (p)
            (-> p last-dir-name
                (member '("pages" "posts" "drafts" "static" "templates")
                        :test #'string=)))
        <>)
       (hv:list-view :title "Source files" :priority 10
                     :display #'last-dir-name)))

(defun last-dir-name (p)
  (-> p pathname-directory last first))

(hv:defview 👀site-file (blog blog)
  (-> blog
      blog-site-dir
      hv:👀items
      (hv:rename :title "Site files" :priority 11)))
