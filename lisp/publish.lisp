;;;; Compilation
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

(defun publish-blog (blog)
  (let ((blog-dir (blog-dir blog))
        (site-dir (blog-site-dir blog)))
    (uiop:delete-directory-tree site-dir :validate t :if-does-not-exist :ignore)
    (ensure-directories-exist site-dir)
    (copy-directory (merge-pathnames "templates/css/" blog-dir)
                    (merge-pathnames "css/" site-dir))
    (copy-directory (merge-pathnames "templates/js/" blog-dir)
                    (merge-pathnames "js/" site-dir))
    (copy-directory (merge-pathnames "templates/img/" blog-dir)
                    (merge-pathnames "img/" site-dir))
    (copy-directory (merge-pathnames "static/" blog-dir)
                    (merge-pathnames "static/" site-dir))
    (uiop:call-with-current-directory site-dir
      #'(lambda ()
          (publish-posts blog)
          (publish-pages blog)
          (publish-tag-indexes blog)
          (publish-month-indexes blog)
          (publish-numeric-indexes blog)
          (publish-feeds blog)))
    (let ((recent-posts (reduce #'(lambda (a b)
                                    (if (< (parse-integer (index-name a))
                                           (parse-integer (index-name b)))
                                        a b))
                                (find-content-of-type blog 'numeric-index))))
      (cl-fad:copy-file (merge-pathnames (page-url recent-posts) site-dir)
                        (merge-pathnames "index.html" site-dir)
                        :overwrite t))
    (add-pagefind blog)))

(defun copy-directory (source destination)
  (assert (uiop:absolute-pathname-p source))
  (assert (uiop:absolute-pathname-p destination))
  (assert (uiop:directory-pathname-p source))
  (assert (uiop:directory-pathname-p destination))
  (ensure-directories-exist destination)
  (fad:walk-directory source
                      #'(lambda (pathname)
                          (unless (equal pathname source)
                            (let* ((relative-path (uiop:subpathp pathname source))
                                   (target (merge-pathnames relative-path
                                                            destination)))
                              (if (uiop:directory-pathname-p pathname)
                                  ;; Ensure an equivalent directory exists
                                  (ensure-directories-exist target)
                                  ;; Copy the absolute source file to the target
                                  (uiop:copy-file pathname target)))))
                      :directories :breadth-first
                      :follow-symlinks nil)
  destination)

(defun publish-posts (blog)
  (loop for (next post prev) on (append '(nil) (posts-sorted-by-date blog))
        while post
        do (write-document blog post nil :prev prev :next next)))

(defun publish-pages (blog)
  (dolist (page (find-content-of-type blog 'page))
    (write-document blog page)))

(defun publish-tag-indexes (blog)
  (dolist (index (find-content-of-type blog 'tag-index))
    (write-document blog index)))

(defun publish-month-indexes (blog)
  (dolist (index (find-content-of-type blog 'month-index))
    (write-document blog index)))

(defun publish-numeric-indexes (blog)
  (let ((indexes (sort (find-content-of-type blog 'numeric-index)
                       #'< :key #'(lambda (i) (parse-integer (index-name i))))))
    (loop for (next index prev) on (append '(nil) indexes)
          while index
          do (write-document blog index nil :prev prev :next next))))

(defun publish-feeds (blog)
  (dolist (feed (find-content-of-type blog 'feed))
    (write-document blog feed (template-fn (feed-format feed)))))

(defun write-document (blog document &optional template-fn &rest render-args)
  "Write the given DOCUMENT to disk as HTML. If TEMPLATE-FN is present,
use it as the template passing any RENDER-ARGS."
  (let ((html (if (or template-fn render-args)
                  (apply #'render-page blog document template-fn render-args)
                  (render-page blog document nil)))
        (url (namestring (page-url document))))
    (write-file (merge-pathnames url (blog-site-dir blog)) html)))

(defun render-page (blog content &optional template-fn &rest render-args)
  "Render the given CONTENT to HTML using TEMPLATE-FN if supplied.
Additional args to render CONTENT can be passed via RENDER-ARGS."
  (funcall (or template-fn (template-fn 'base))
           (list :config *blog*
                 :content content
                 :raw (apply 'render blog content render-args)
                 :pubdate (date-as-rfc3339-timestring content)
                 :injections nil ;; (find-injections content)
                 )))

(defun date-as-rfc3339-timestring (content)
  (if-let (date (content-date content))
    (destructuring-bind (year month day)
        (str:split #\- date)
      (local-time:to-rfc3339-timestring
       (local-time:encode-timestamp 0 0 0 0
                                    (parse-integer day)
                                    (parse-integer month)
                                    (parse-integer year)
                                    :timezone local-time:+utc-zone+)))
    (local-time:to-rfc3339-timestring (local-time:now))))

(defun write-file (path text)
  "Write the given TEXT to PATH. PATH is overwritten if it exists and created
along with any missing parent directories otherwise."
  (ensure-directories-exist path)
  (with-open-file (out path
                   :direction :output
                   :if-exists :supersede
                   :if-does-not-exist :create
                   :external-format :utf-8)
    (write text :stream out :escape nil)))

(defgeneric render (blog document &key &allow-other-keys)
  (:documentation "Render the given DOCUMENT to HTML."))

(defmethod render (blog (object post) &key prev next)
  (funcall (template-fn 'post) (list :config *blog*
                                     :post object
                                     :prev prev
                                     :next next)))

(defmethod render (blog (object page) &key next prev)
  (declare (ignore next prev))
  (funcall (template-fn 'post) (list :config *blog*
                                     :post object)))

(defmethod render (blog (object index) &key prev next)
  (funcall (template-fn 'index) (list :tags (find-content-of-type blog 'tag-index)
                                      :months (find-content-of-type blog 'month-index)
                                      :config *blog*
                                      :index object
                                      :prev prev
                                      :next next)))

(defun add-pagefind (blog)
  (uiop:call-with-current-directory (blog-site-dir blog)
    #'(lambda ()
        (uiop:run-program "npx -y pagefind --site ."))))

(defun copy-to-server (blog)
  (uiop:call-with-current-directory (blog-site-dir blog)
    #'(lambda ()
        (uiop:run-program "rsync -avz --delete -e ssh --progress --exclude '.well-known' ./ site-host:blog.khinsen.net"))))
