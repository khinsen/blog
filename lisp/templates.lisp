;;;; Templates
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

(defvar *template-path* (asdf:system-relative-pathname :kh-blog "templates/"))

(defun compile-templates ()
  (let ((templates
          (remove-if-not #'(lambda (p)
                             (string= "tmpl" (pathname-type p)))
                         (uiop:directory-files *template-path*))))
    (dolist (template templates)
      (closure-template:compile-template :common-lisp-backend template))))

(defun template-fn (name)
  (find-symbol (princ-to-string name) (find-package 'kh-blog.templates)))
