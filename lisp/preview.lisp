;;;; Preview
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

;;
;; Preview server
;;

(defvar *server* nil)

(defun ensure-preview-server (blog port)
  (unless *server*
    (let* ((site-dir (blog-site-dir blog))
           (acceptor (make-instance 'hunchentoot:acceptor
                                    :port port
                                    :document-root site-dir)))
      (setf *server* acceptor)
      (hunchentoot:start acceptor))))
