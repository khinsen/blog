;;;; DOI verification
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:kh-blog)

(defun url-from-doi (doi)
  (let ((url (str:concat "https://doi.org/" doi)))
    (multiple-value-bind (body status response-headers)
        (dex:get url :max-redirects 0)
      (declare (ignore body))
      (assert (= status 302))
      (gethash "location" response-headers))))

(defun check-doi (post domain)
  (let* ((post-url (str:concat domain (princ-to-string (page-url post))))
         (post-doi (when (slot-boundp post 'doi) (doi post))))
    (if post-doi
        (let* ((doi-url (url-from-doi post-doi))
               (ok (string= post-url doi-url)))
          (if ok
              `(:ok ,post)
              `(:mismatch ,post ,post-url ,doi-url)))
        `(:missing-doi ,post))))

(defun check-all-dois ()
  (let* ((blog (load-blog))
         (domain (blog-domain blog))
         (posts (posts-sorted-by-date *blog*)))
    (dolist (post posts)
      (let ((report (check-doi post domain)))
        (case (car report)
          (:ok (format t "~&OK: ~A~%" (title-of post)))
          (:mismatch (format t "~&Mismatch: ~A~%Post URL: ~A~%DOI URL: ~A~%"
                             (title-of post) (third report) (fourth report)))
          (:missing-doi (format t "~&No DOI: ~A~%" (title-of post))))))))
