;;;; Package definition
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(defpackage :kh-blog
  (:use :cl)
  (:local-nicknames (:hv :html-inspector-views))
  (:import-from :alexandria
   :if-let :when-let :compose)
  (:import-from :arrow-macros
   :-> :-<> :->> :-<>> :<> :some-> :some->>)
  (:export #:*blog*))
