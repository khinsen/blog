;; Rename the posts imported from Wordpress (via the import tool that
;; comes with coleslaw) to match the naming convention of the
;; Frog-based blog, where post filenames start with a time stamp.
;;
;; This is a one-off script, so it's neither polished nor decently documented.
;; It's here for others who may find inspiration in it.

(ql:quickload "cl-ppcre")
(ql:quickload :coleslaw-cli)
(ql:quickload :arrows)
(ql:quickload :str)

(defvar *wordpress-imports*
  (remove-if-not (lambda (filename)
                   (and (equal (pathname-type filename) "post")
                        (not (ppcre:scan "^\\d\\d\\d\\d-\\d\\d-\\d\\d-.*" (pathname-name filename)))))
                 (uiop:directory-files "./")))

(defun date (filename)
  (arrows:-<> filename
              (coleslaw::read-content)
              (getf :date)
              (str:split " " <>)
              (first)))

(defun date-stamped-name (filename)
  (make-pathname
   :directory (pathname-directory filename)
   :name (str:concat (date filename) "-" (pathname-name filename))
   :type (pathname-type filename)))

(dolist (filename *wordpress-imports*)
  (rename-file filename (date-stamped-name filename)))
