;; Convert the posts of my old Frog-based blog into the coleslaw format.
;; No changes are made to the bodies of the posts, which is Markdow text,
;; but the metadata header lines need to be adapted.
;;
;; This is a one-off script, so it's neither polished nor decently documented.
;; It's here for others who may find inspiration in it.

(ql:quickload :serapeum)
(ql:quickload :cl-change-case)
(ql:quickload :str)

(defvar *post-directory* (merge-pathnames #P"Repos/blog/_src/posts/"
                                          (user-homedir-pathname)))

(defvar *all-posts*
    (remove-if-not (lambda (filename)
                     (equal (pathname-type filename) "md"))
                   (uiop:directory-files *post-directory*)))

(defun metadata-line? (s)
  (str:starts-with? "    " s))

(defun metadata-lines (s)
  (serapeum:take-while #'metadata-line? s))

(defun translate-date (s)
  (if (str:starts-with? "date:" s)
      (str:replace-first "T" " " s)
      s))

(defun translate-metadata-line (s)
  (translate-date
   (change-case:lower-case-first
    (subseq s 4))))

(defun import-post (file)
  (let ((imported-post (make-pathname :name (pathname-name file) :type "post"
                                      :directory '(:relative "import"))))
    (with-open-file (output imported-post
                            :direction :output
                            :if-exists :supersede)
      (let* ((content (uiop:read-file-lines file))
             (metadata (metadata-lines content))
             (body (serapeum:drop (length metadata) content)))
        (format output ";;;;;~%")
        (dolist (line metadata)
          (format output "~a~%" (translate-metadata-line line)))
        (format output "format: md~%")
        (format output ";;;;;~%")
        (dolist (line body)
          (format output "~a~%" line))
        (format output "~%<!-- Local Variables: -->~%<!-- mode: markdown -->~%<!-- End: -->~%")))))

(dolist (post *all-posts*)
  (import-post post))
