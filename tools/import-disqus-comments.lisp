;; Convert exported comments from Disqus (file "disqus-comments.xml")
;; into HTML snippets ready to be pasted into the blog posts.
;;
;; This is a one-off script, so it's neither polished nor decently documented.
;; It's here for others who may find inspiration in it. I have no plans to
;; use Disqus again in the future.

(ql:quickload :plump)
(ql:quickload :plump-sexp)
(ql:quickload :str)
(ql:quickload :arrows)

(defun read-comments (filename)
  (with-open-file (input filename
                         :direction :input
                         :if-does-not-exist nil)
    (and input
         (let ((plump:*tag-dispatchers* plump:*xml-tags*))
           (plump:parse input)))))

(defun first-element-by-tag-name (xml tag)
  (arrows:some-> xml
                 (plump:get-elements-by-tag-name tag)
                 (first)))

(defvar *disqus-export* (read-comments "disqus-comments.xml"))

(plump-sexp:serialize *disqus-export*)

(defclass thread ()
  ((id :reader thread-id :initarg :id)
   (link :reader link :initarg :link)
   (title :reader title :initarg :title)
   (posts :accessor posts :initform nil)))

(defun make-thread (xml)
  (make-instance 'thread
                 :id (plump:attribute xml "dsq:id")
                 :link (plump:text
                        (first-element-by-tag-name xml "link"))
                 :title (plump:text
                        (first-element-by-tag-name xml "title"))))

(defclass post ()
  ((id :reader post-id :initarg :id)
   (thread-id :reader thread-id :initarg :thread-id)
   (parent-id :reader parent-id :initarg :parent-id)
   (message :reader message :initarg :message)
   (author :reader author :initarg :author)
   (replies :accessor replies :initform nil)))

(defun make-post (xml)
  (make-instance 'post
                 :id (plump:attribute xml "dsq:id")
                 :thread-id (arrows:some-> xml
                                           (first-element-by-tag-name "thread")
                                           (plump:attribute "dsq:id"))
                 :parent-id (arrows:some-> xml
                                           (first-element-by-tag-name "parent")
                                           (plump:attribute "dsq:id"))
                 :message (plump:text
                           (first-element-by-tag-name xml "message"))
                 :author (plump:text
                          (first-element-by-tag-name
                           (first-element-by-tag-name xml "author")
                           "name"))))

(defun select-children-with-tag (xml tag)
  (coerce
   (remove-if-not #'(lambda (node) (and (plump:element-p node)
                                        (equal tag (plump:tag-name node))))
                  (plump:children xml))
   'list))

(defvar *threads*
  (mapcar #'make-thread
          (select-children-with-tag (plump:first-element *disqus-export*) "thread")))

(defvar *posts*
  (mapcar #'make-post
          (select-children-with-tag (plump:first-element *disqus-export*) "post")))

(defun is-comment-on-post? (thread)
  (str:starts-with? "http://blog.khinsen.net/posts" (link thread)))

(defvar *blog-threads*
  (remove-if-not #'is-comment-on-post? *threads*))

(defvar *posts-by-id* (make-hash-table :test #'equal))
(dolist (post *posts*)
  (setf (gethash (post-id post) *posts-by-id*) post))

(defvar *threads-by-id* (make-hash-table :test #'equal))
(dolist (thread *blog-threads*)
  (setf (gethash (thread-id thread) *threads-by-id*) thread))

(dolist (post *posts*)
  (if (parent-id post)
      (let ((parent (gethash (parent-id post) *posts-by-id*)))
        (when parent
          (push post (replies parent))))
      (let ((thread (gethash (thread-id post) *threads-by-id*)))
        (when thread
          (push post (posts thread))))))

(defvar *non-empty-threads*
  (remove-if-not #'posts *blog-threads*))

(defun comment-tree (stream posts)
  (when posts
    (format stream "<ul>~%")
    (dolist (post posts)
      (format stream "<li><i>~a:</i>~a"
              (author post)
              (message post))
      (comment-tree stream (replies post))
      (format stream "</li>~%"))
    (format stream "</ul>~%"))
)
(defun comments (stream thread)
  (format stream "~a~%~%" (link thread))
  (format stream "<h3>Comments retrieved from Disqus</h3>~%")
  (comment-tree stream (posts thread)))

(with-open-file (output "comments.txt"
                        :direction :output
                        :if-exists :supersede)
  (dolist (thread *non-empty-threads*)
    (format output "-----------~%")
    (comments output thread)
    (format output "-----------~%")))
