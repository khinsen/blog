;;;; System definition
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(asdf:defsystem #:kh-blog
  :description "Code for building my blog at http://blog.khinsen.net/"
  :author "Konrad Hinsen <konrad.hinsen@fastmail.net>"
  :license  "GPL"
  :version "0.0.1"
  :serial t
  :depends-on (#:closure-template
               #:3bmd
               #:3bmd-ext-code-blocks
               #:alexandria
               #:arrow-macros
               #:local-time
               #:cl-unicode
               #:cl-fad
               #:dexador
               #:asdf
               #:uiop
               #:html-inspector-views
               #:hunchentoot)
  :components ((:file "lisp/package")
               (:file "lisp/templates")
               (:file "lisp/blog")
               (:file "lisp/content")
               (:file "lisp/load-pages")
               (:file "lisp/tags")
               (:file "lisp/indexes")
               (:file "lisp/feeds")
               (:file "lisp/publish")
               (:file "lisp/preview")
               (:file "lisp/doi")))
