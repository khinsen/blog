;;;;;
title: About
;;;;;

This blog is mostly about topics related to my work as a researcher in computational biophysics. However, all the views expressed here are my own and not my employer's.

There are a few other places where you can find me or my work:

- Check out my [home page](http://khinsen.net/)
- Contact me [on Mastodon](https://scholar.social/@khinsen) or [on BlueSky](https://bsky.app/profile/khinsen.net) (I am much more active on Mastodon)
- See also my digital garden on [Science in the Digital Era](https://science-in-the-digital-era.khinsen.net/)

If you want to subscribe to my blog in your RSS reader, here are the feeds (which should also be auto-discoverable, but you know, computers...):

- [RSS](/feeds/all.rss.xml)
- [Atom](/feeds/all.atom.xml)

Finally, if you wonder how this blog is produced, you can take a look at its [public repository](https://codeberg.org/khinsen/blog) which contains the source code for all the content but also the static site generator used to assemble everything. The site generator is [situated software](https://science-in-the-digital-era.khinsen.net/#Situated%20software), written by myself for my own use. It is not meant to be usable for *your* blog, and it has no user documentation either. But feel free to draw inspiration from it, or even adapt it to your own needs!

<!-- Local Variables: -->
<!-- mode: markdown -->
<!-- End: -->
